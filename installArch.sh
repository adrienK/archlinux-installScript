#!/bin/sh

### INFO
### CONFIG

#Dispositon du clavier
keyMap="fr"

#Taille de la partition root en Go
rootSize=32

#Utilisation du wifi
wifi="true"

#Si il y a une carte graphique nvidia
nvidia="true"

#Reinitialisation profonde, pour carte SD, cl usb ou disque vérollé
sector0="false"

#Utiliser le disque avec un swap
makeSwap="false"

#Utiliser un UEFI
useEFI="true"

### VAR
pac_conf="/etc/pacman.conf"

pac_s_nvidia="nvidia nvidia-utils nvidia-settings libva-vdpau-driver"
pac_s_wifi="iw rfkill wireless_tools wpa_supplicant ipw2100-fw ipw2200-fw linssid network-manager-applet"
pac_s_efi="efibootmgr"
pac_s="wireshark-cli wireshark-common wireshark-qt xorg-server xorg-xinit xf86-video-nouveau nouveau-dri lib32-glu lib32-mesa sakura steam steam-native-runtime thunderbird tmux virtualbox vlc wine pulseaudio-alsa pamixer htop cmake lightdm calc cronie avahi openntpd sshfs xorg-fonts-type1 ttf-dejavu artwiz-fonts font-bh-ttf font-bitstream-speedo p7zip abiword arduino arduino-builder arduino-ctags audacity blender cheese ttf-arphic-uming ttf-baekmuk cups jwm pulseaudio numlockx xsane pavucontrol lshw mpv mumble nitrogen libnotify okular pcmanfm pcre perl php python rust chromium darktable deluge dolphin-emu filezilla firefox flashplugin gimp git smartmontools glu mesa xf86-video-vesa xf86-video-ati xf86-video-intel dialog gnumeric gparted gpicview gprename gsmartcontrol gthumb gufw gvim icedtea-web inkscape jre9-openjdk jre9-openjdk-headless leafpad libreoffice lmms gsfonts sdl_ttf ttf-bitstream-vera ttf-cheapskate ttf-liberation ttf-freefont gtk3-print-backends arandr cbatticon volumeicon pamixer util-linux grub libinput mesa xorg lightdm-gtk-greeter lightdm-gtk-greeter-settings numlockx xterm gvfs"

#yaourt_s="skype google-chrome xdgmenumaker shutter tkpacman"

### FUNCTIONS
checkRoot () {
  if [ $(id -u) -eq 0 ]; then
      echo "| Compte root valide"
  else
    echo "|ERR! Vous devez étre en utilisateur root"
    exit 1
  fi
}

checkIp () {
    ping -q -c5 google.com > /dev/null
     
    if [ $? -eq 0 ]; then
        echo "| Connection internet etablie"
    else
        echo "|ERR! Pas de connections disponible, vérifiez votre branchement ethernet."
        exit 1
    fi
}

checkTime () {
    timedatectl set-ntp true
    echo "| $(timedatectl | grep 'Local' | cut -d' ' -f7-)"
}

selectDrive () {
    echo "| Choix du disque
| ---------------
Merci de choisir le disque à utiliser pour l'installation !
Celui ci serat entiérement formaté (vidé)

$(lsblk -ido NAME,TYPE,SIZE,MODEL)
";

    while true; do
        echo -n "Nom du Disque [ENTER] (ex: sdb): "
        read -r used_drive
    if [ -b "/dev/${used_drive}" ]; then break; fi
    done

    echo "| ---------------"
}

initDrive () {
    # Mise a zero
    if [ "${sector0}" = "true" ]; then
        echo -e "| Initialisation du disque à 0 (Trés long)\n| ..."
        dd "if=/dev/zero" "of=/dev/${used_drive}" "bs=16M" "conv=notrunc"
    fi
    
    # Nouvelle table de partition
    echo -en "| Nouvelle table de partition\n| ..."
    if [ "${useEFI}" = "true" ]; then
        echo -r "g\nw" | fdisk "/dev/${used_drive}" &>/dev/null
    else
        echo -r "o\nw" | fdisk "/dev/${used_drive}" &>/dev/null
    fi
    echo " Ok"

    # Partitionnement 
    echo -en "| Partitionnement de ${used_drive}\n| ..."
    #Boot
    if [ "${useEFI}" = "true" ]; then
        echo ',+300M,1' | sfdisk --no-reread -a "/dev/${used_drive}" &>/dev/null
    else
        echo ',+300M,L' | sfdisk --no-reread -a "/dev/${used_drive}" &>/dev/null
    fi
    #Root
    echo ',+'${rootSize}'G,L' | sfdisk --no-reread -a "/dev/${used_drive}" &>/dev/null
    #Swap
    if [ "${makeSwap}" = "true" ]; then
        echo ',+2G,S' | sfdisk --no-reread -a "/dev/${used_drive}" &>/dev/null
    fi
    #Home
    echo ',,L' | sfdisk --no-reread -a "/dev/${used_drive}" &>/dev/null
    echo " Ok"

    # Définition des emplacements
    part=1
    part_boot="/dev/${used_drive}${part}"; ((part++))
    part_root="/dev/${used_drive}${part}"; ((part++))
    if [ "${makeSwap}" = "true" ]; then
        part_swap="/dev/${used_drive}${part}"; ((part++))
    fi
    part_home="/dev/${used_drive}${part}"

    # Formatage
    if [ "${useEFI}" = "true" ]; then
        formatPart ${part_boot} "BootEFI"
    else
        formatPart ${part_boot} "Boot"
    fi
    formatPart ${part_root} "Root"
    if [ "${makeSwap}" = "true" ]; then
        formatPart ${part_swap} "Swap"
    fi
    formatPart ${part_home} "Home"
    
    # SMART
    echo "| Activation de SMART (surveillance disques)"
    smartctl --smart=on --offlineauto=on --saveauto=on "/dev/${used_drive}"
}

mountDrive () {
    mount_point="/mnt/${used_drive}"

    echo "| Création du point Root"
    mkdir -p "${mount_point}"
    echo -en "| Montage de Root\n| ..."
    mount "${part_root}" "${mount_point}"
    echo " Ok"

    echo -en "| Création des autres points de montage\n| ..."
    if [ "${useEFI}" = "true" ]; then
        mkdir -p "${mount_point}/boot/efi"
    else
        mkdir "${mount_point}/boot"
    fi
    mkdir "${mount_point}/home"
    echo " Ok"

    echo -en "| Montage des points\n| ..."
    if [ "${useEFI}" = "true" ]; then
        mount  -t vfat "${part_boot}" "${mount_point}/boot/efi"
    else
        mount "${part_boot}" "${mount_point}/boot"
    fi
    mount "${part_home}" "${mount_point}/home"
    echo " Ok"

    if [ "${makeSwap}" = "true" ]; then
        echo -en "| Activation du Swap\n| ..."
        swapon "${part_swap}"
        echo " Ok"
    fi
}

formatPart () {
    if [ -b "${1}" ]; then
        echo -en "| Formatage du ${2}\n| ..."

        if [ "${2}" = "BootEFI" ]; then
            mkfs.vfat -F32 "${1}" &>/dev/null
        elif [ "${2}" = "Swap" ]; then
            mkswap "${1}" &>/dev/null
        else
            mkfs.ext4 "${1}" &>/dev/null
        fi

        if [ "$(file -sL ${1})" = "${1}: data" ]; then
            echo "|ERR! Probléme de formatage"
            echo "| Utilisez l'option sector0 à true avant de relancer le script"

            exit 1
        fi

        echo " Ok"
    else
        echo "|ERR! La partition ${1} est introuvable"
        echo "| Utilisez l'option sector0 à true avant de relancer le script"
        
        exit 1
    fi

    sleep 1
}

installBase () {
    echo "| Installation du système de base"
    pacstrap "${mount_point}" base base-devel
    echo "| Generation du fichier de map"
    genfstab -U -p "${mount_point}" >> "${mount_point}/etc/fstab"
}

#installYaourt () {
#    echo '
#    [archlinuxfr]
#    SigLevel = Never
#    Server = http://repo.archlinux.fr/$arch' >> $pac_conf
#
#    sPacman yaourt
#}

sPacman () {
    pacman -Sy --noconfirm --noprogressbar --quiet ${1}
}

#sYaourt () {
#    #merde ...
#    yaourt -S $1
#}

addUser () {
    echo -n "| Nom du nouvel utilisateur (Uniquement lettres):"
    read -r userName
    useradd -g users -G scanner,sys,storage,power -m "${userName}"
    passwd "${userName}"
    echo "| L'utilisateur ${userName} est activé"
}

### MAIN
if [ "${1}" = "start" ]; then
clear
    echo "|| Debut de l'installation"

    checkRoot
    checkIp
    checkTime

    selectDrive
    initDrive
    mountDrive
    
    echo ${used_drive} > /mnt/${used_drive}/root/drive

    installBase
    cp "./installArch.sh" "${mount_point}/root/"

    echo -en "|| Etape 2\n| Passage en Chroot dans 5s\n| "
    sleep 1; echo -n ".";sleep 2; echo -n ".";sleep 2; echo ".";
    arch-chroot "${mount_point}" /root/installArch.sh continue
elif [ "${1}" = "continue" ]; then
    echo "|| Nouveau système"    

    echo -n "| Nom de la machine (Uniquement lettres et chiffres): "
    read -r cpName
    echo "${cpName}" > /etc/hostname
    echo "127.0.1.1 ${cpName}.localdomain ${cpName}" >> /etc/hosts

    echo "| Heure local sur la zone de Paris"
    ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime

    echo "| Utilisation de la locale 'fr_FR.UTF-8 UTF-8'"
    echo LANG="fr_FR.UTF-8" > /etc/locale.conf
    export LANG=fr_FR.UTF-8
    echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen
    locale-gen

    echo "| Disposition du clavier: ${keyMap}"
    echo "KEYMAP=${keyMap}" > /etc/vconsole.conf

    echo "| Génération des RAMdisks"
    mkinitcpio -p linux

    echo "| Définition du password Root"
    passwd

    echo "| Ajout d'un utilisateur"
    while true; do
        addUser
        echo -n "| Voulez vous ajouter un autre utilisateur (o)ui / (n)on: "
        read -r newUser
        if [ "${newUser}" = "n" ]; then break; fi
    done

#    Ajout de multilib en safe
    echo "[multilib]" >> /etc/pacman.conf
    echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf
    
#    installation des pkg en plus
    echo "| Installation des logiciels complémentaires"
    sPacman "${pac_s}"

#    configuration des logiciel en global
    echo "| Activations des deamons"
    systemctl enable dhcpcd #automatisation du reseau
    systemctl enable avahi-daemon #usine a plein de truc
    systemctl enable lightdm #gestionnaire de connections
    systemctl enable cronie #gestion des taches
    systemctl enable org.cups.cupsd #serveur d'impression
    systemctl enable smartd #contrôle l'état des disques durs
    systemctl enable ufw #configuration "automatique" du pare-feu
    systemctl enable ntpd #date/heure

    if [ "${wifi}" = "true" ]; then
        echo "| Install/Conf des utilitaires wifi"
        sPacman "${pac_s_wifi}"
        systemctl disable dhcpcd
        systemctl enable NetworkManager
    fi

    if [ "${nvidia}" = "true" ]; then
        echo "| Install des drivers nvidia"
        sPacman "${pac_s_nvidia}"
        
        echo "Section \"Device\"" >> "/etc/X11/xorg.conf.d/20-nvidia.conf"
        echo "    Identifier     \"Device0\"" >> "/etc/X11/xorg.conf.d/20-nvidia.conf"
        echo "    Driver         \"nvidia\"" >> "/etc/X11/xorg.conf.d/20-nvidia.conf"
        echo "    VendorName     \"NVIDIA Corporation\"" >> "/etc/X11/xorg.conf.d/20-nvidia.conf"
        echo "    Option \"RegistryDwords\" \"EnableBrightnessControl=1\"" >> "/etc/X11/xorg.conf.d/20-nvidia.conf"
        echo "EndSection" >> "/etc/X11/xorg.conf.d/20-nvidia.conf"
    fi
    
    if [ "${useEFI}" = "true" ]; then
        echo "| Install pour EFI"
        sPacman "${pac_s_efi}"
    fi
    
#    Clavier azerty
    echo "| Configuration clavier FR"

    echo "Section \"InputClass\"" >> "/etc/X11/xorg.conf.d/10-keyboard-layout.conf"
    echo "    Identifier         \"Keyboard Layout\"" >> "/etc/X11/xorg.conf.d/10-keyboard-layout.conf"
    echo "    MatchIsKeyboard    \"yes\"" >> "/etc/X11/xorg.conf.d/10-keyboard-layout.conf"
    echo "    Option             \"XkbLayout\"  \"fr\"" >> "/etc/X11/xorg.conf.d/10-keyboard-layout.conf"
    echo "    Option             \"XkbVariant\" \"latin9\"" >> "/etc/X11/xorg.conf.d/10-keyboard-layout.conf"
    echo "EndSection" >> "/etc/X11/xorg.conf.d/10-keyboard-layout.conf"

#    grub
    echo "| Install du bootloader"    
    
    if [ "${useEFI}" = "true" ]; then
        mkdir -p /boot/efi/EFI
        grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=arch_grub --recheck
    else
        grub-install --target=i386-pc /dev/`cat /root/drive`
    fi
    
    grub-mkconfig -o /boot/grub/grub.cfg

    exit
else
    echo "Démmarer l'installation avec ./installArch.sh start"
    
    exit
fi

umount -R ${mount_point}

if [ "${makeSwap}" = "true" ]; then
    swapoff "${part_swap}"
fi

echo "|| Installation terminé"
exit
